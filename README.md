# GatherMaster / PUBobot-discord fork
This fork of [PUBobot-discord](https://gitlab.com/pubobot-discord/PUBobot-discord) known as GatherMaster. This is a multichannel discord bot for pickup games organisation. 
Bot keeps statistic database, have features to automatically remove AFK users, disallow users to play pickups, fun phrases, teams picking and more! 

## Main changes compared to original Pubobot: 
 - [Party system](https://gitlab.com/mittermichal/PUBobot-discord/-/blob/master/commands.md#party-system)
 - [Game servers](https://gitlab.com/mittermichal/PUBobot-discord/-/blob/master/commands.md#game-servers)
 - [Status message](https://gitlab.com/mittermichal/PUBobot-discord/-/blob/master/commands.md#status-message)
 - `!subfor` can be only done by mods
 - `!pick_captains` to manually change captains for mods
 - adding while in active match possible - moves player to separate queue that will fill the pickup queue after the match is finished
 - scheduled adding
 - map pick/ban
 - permanent noadds, anonymous noadds
 - automatic noadd upon aborting or not readying up
 - `!report_match` command
 - more `pick_captains` options
 - option to hide rating in channel
 - permanent [TrueSkill](https://trueskill.org/) rating
 - `!activity` command

Original Pubobot has been updated and moved by Leshaka to https://github.com/Leshaka/PUBobot2 with a lot of new stuff and dashboard.

# Requirements
Python 3.7+, discord.py, Quart, see requirements.txt
# Running
- pip install -r requirements.txt
- Fill your discord bot token or username and password in the 'config.cfg' file.
- `cp client_config.example.py client_config.py` - set INTENTS_PRESENCES to False in client_config.py if u don't have privileged presence intent enabled https://discordpy.readthedocs.io/en/latest/intents.html
- you need to enable message content intent in discord bot settings
- Run './pubobot.py' to launch the program.
- Invite your bot to the desired server.
- Type '!enable_pickups' on the desired channel.

# Getting help
Gathermaster development server: https://discord.gg/tTEDFfNu5f

Original Pubobot developement server: https://discord.gg/rjNt9nC

# Credits
Developer: Leshka. You can contact me via e-mail leshkajm@ya.ru, on discord (Leshaka#8570) or pm 'Leshaka' on irc.quakenet.org server.   
Help: #warsow.pickup admin team.   
Special thanks to Mute and Perrina.

# License
Copyright (C) 2015 Leshka.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See 'GNU GPLv3.txt' for GNU General Public License.
