from modules.rating import get_balanced_teams
from trueskill import Rating


def test_balanced_teams():
    players = {
        1: Rating(mu=35, sigma=1),
        2: Rating(mu=30, sigma=1),
        3: Rating(mu=25, sigma=1),
        4: Rating(mu=20, sigma=1)
    }
    teams, quality = (set(team) for team in get_balanced_teams(players))
    assert {1, 4} in teams
    assert {2, 3} in teams
    assert quality > 0.9
