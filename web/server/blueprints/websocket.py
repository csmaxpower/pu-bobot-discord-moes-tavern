import asyncio
import json
from time import time

from oauthlib.oauth2 import InvalidGrantError
from quart import Blueprint, request, abort, jsonify, copy_current_websocket_context, websocket, current_app, session, \
    redirect, render_template
from .utils import local_request

app = Blueprint('WebSocket', __name__)


class WebSocketClient:
    def __init__(self, queue, user_id):
        self.queue = queue
        self.user_id = user_id
        self.channels = set()
        self.last_add_time = None


@app.route('/ws_test', methods=['GET'])
async def ws_test():
    return await render_template('index.html')


@app.route('/', methods=['GET'])
async def index():
    return redirect(current_app.config["FRONTEND_URL"])


async def consumer(client, data):
    if data['action'] == 'ready':
        status = await current_app.ipc_client.request("set_ready", user_id=client.user_id, is_ready=data['is_ready'])
        await client.queue.put({'event': 'ready_response', 'status': status})  # TODO: is this needed?
    elif data['action'] == 'add':
        if client.last_add_time is None or int(time()) - client.last_add_time > current_app.config["ADD_TIME_LIMIT"]:
            client.last_add_time = int(time())
            status = await current_app.ipc_client.request(
                "add", user_id=client.user_id, channel_id=int(data['channel_id']), pickup_name=data['pickup_name']
            )
        else:
            status = {'error': 'remove limit'}
        await client.queue.put({'event': 'add_response', 'status': status})
    elif data['action'] == 'remove':
        if client.last_add_time is None or int(time()) - client.last_add_time > current_app.config["ADD_TIME_LIMIT"]:
            client.last_add_time = int(time())
            status = await current_app.ipc_client.request(
                "remove", user_id=client.user_id, channel_id=int(data['channel_id']), pickup_name=data['pickup_name']
            )
        else:
            status = {'error': 'remove limit'}
        await client.queue.put({'event': 'add_response', 'status': status})
    elif data['action'] == 'get_user_pickups':
        response = (await current_app.ipc_client.request(
            "get_user_pickups", user_id=client.user_id,
            channel_id=int(data['channel_id']), index=max(data['index'], 0)
        )).get('data', [])
        response.reverse()
        await client.queue.put({'event': 'user_pickups',
                                'data': response,
                                'channel_id': data['channel_id'],
                                'index': data['index']}
                               )
    elif data['action'] == 'get_leader_board':
        response = (await current_app.ipc_client.request(
            data['action'], user_id=client.user_id,
            channel_id=int(data['channel_id']), index=max(data['index'], 0)
        )).get('data', [])
        await client.queue.put({'event': 'leader_board',
                                'data': response,
                                'channel_id': data['channel_id'],
                                'index': data['index']}
                               )
    elif data['action'] == 'pick_player':
        status = await current_app.ipc_client.request(
            "pick_player", user_id=client.user_id, pick_user_id=data['player_id']
        )
        await client.queue.put({'event': data['action'] + '_response', 'status': status})


async def producer(queue):
    while True:
        data = await queue.get()
        yield data


async def register(client):
    # print("client registered")
    current_app.connected_websocket_clients.add(client)
    pickups = (await current_app.ipc_client.request("get_pickups", user_id=client.user_id)).get('data', [])
    # pickups = []
    match = await current_app.ipc_client.request("get_match", user_id=client.user_id)
    stats = (await current_app.ipc_client.request("get_user_stats", user_id=client.user_id)).get('data', [])

    def add_index_to_channel_stats(channel_stats):
        channel_stats['index'] = 0
        channel_stats['channel_id'] = str(channel_stats['channel_id'])
        channel_stats['pickups'].reverse()
        return channel_stats

    # for pickup in pickups:
    #     client.channels.add(pickup['channel_id'])
    await client.queue.put({
        'event': 'state',
        'pickups': pickups,
        'user_id': client.user_id
    })
    if match is not None:
        await client.queue.put({
            'event': 'match_change',
            'match': match
        })
    await client.queue.put({
        'event': 'stats',
        'stats': [] if stats is None else [add_index_to_channel_stats(stat) for stat in stats],
        'user_id': client.user_id
    })
    # await notify_users()


async def unregister(client: WebSocketClient):
    print("client unregistered")
    current_app.connected_websocket_clients.remove(client)
    # await notify_users()


# https://stackoverflow.com/questions/60732592/bidirectional-communiation-with-websockets-in-quart
@app.websocket('/ws')
async def handler():
    token = session.get('auth_token', None)
    # TODO: CSRF?
    if token is None or not current_app.redis_client.sismember('auth_tokens', token):
        error_message = 'not logged in'
        await websocket.send(json.dumps(error_message))
        return jsonify(error_message), 403  # or abort(403)
    # TODO: try catch?
    try:
        user = await current_app.discord.fetch_user()
    except InvalidGrantError:
        error_message = 'invalid grant'
        current_app.redis_client.srem('auth_tokens', token)
        await websocket.send(json.dumps(error_message))
        return jsonify(error_message), 400
    queue = asyncio.Queue()
    client = WebSocketClient(queue, user.id)
    await register(client)

    async def producer_handler():
        async for message in producer(queue):
            await websocket.send(json.dumps(message))

    async def consumer_handler():
        while True:
            data = await websocket.receive()
            await consumer(client, json.loads(data))

    consumer_task = asyncio.ensure_future(
        copy_current_websocket_context(consumer_handler)(),
    )
    producer_task = asyncio.ensure_future(
        copy_current_websocket_context(producer_handler)(),
    )
    try:
        await asyncio.gather(consumer_task, producer_task)
    finally:
        consumer_task.cancel()
        producer_task.cancel()
        await unregister(client)


@app.route('/broadcast', methods=['POST'])
@local_request
async def broadcast():
    data = await request.get_json()
    for client in current_app.connected_websocket_clients:
        await client.queue.put(json.dumps({'event': 'broadcast', 'message': data['message']}))
    return jsonify(True)
