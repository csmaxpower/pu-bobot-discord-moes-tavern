import secrets

from quart import Blueprint, request, abort, current_app, redirect, session, url_for
from quart_discord import requires_authorization, Unauthorized

app = Blueprint('Auth', __name__)


@app.route("/login/")
async def login():
    return await current_app.discord.create_session(scope=["identify"], prompt=False)


# TODO: logout btn
@app.route("/logout/")
async def logout():
    current_app.discord.revoke()
    current_app.redis_client.srem('auth_tokens', session.get('auth_token'))
    return redirect(current_app.config["FRONTEND_URL"])


@requires_authorization
@app.route("/admin/")
async def admin():
    user = await current_app.discord.fetch_user()
    if user.id != current_app.config["ADMIN_USER_ID"]:
        abort(403)
    else:
        return f"""
        clients: {len(current_app.connected_websocket_clients)}
        """


# TODO: {token: {"creation_time": creation time}
@app.route("/callback/")
async def callback():
    print(await request.values)
    await current_app.discord.callback()
    token = secrets.token_hex(32)
    print("token:", token)
    current_app.redis_client.sadd('auth_tokens', token)
    session['auth_token'] = token
    return redirect(current_app.config["FRONTEND_URL"])


@app.errorhandler(Unauthorized)
async def redirect_unauthorized(e):
    return redirect(url_for("Auth.login"))


@app.route("/me/")
@requires_authorization
async def me():
    user = await current_app.discord.fetch_user()
    return f"""
    <html>
        <head>
            <title>{user.name}</title>
        </head>
        <body>
            <div>{user.name} {user.id}</div>
            <img src='{user.avatar_url}' />
        </body>
    </html>"""
