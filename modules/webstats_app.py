import os
import re
from modules import webstats_GameStats
from modules import webstats_config

# set stylesheet from config
stylesheet_path = webstats_config.STYLESHEET_DIR_PATH

# set site specific attributes
page_title = webstats_config.PAGE_TITLE
site_index = webstats_config.INDEX_HOME_PATH
header_image = webstats_config.HEADER_IMAGE_PATH

# set folder path of stats files from LUA output
stats_dir_path = webstats_config.STATFILES_DL_PATH

# set folder path for HTML output
output_dir = webstats_config.OUTPUT_DIR

# used for formatting file names and channel info
matchformat = ""
matchconfigs = webstats_config.CONFIG_DISPLAY_FORMAT

def build_stats(finished_matchID):
    # stores list of maps played during a match
    maplist = []

    # list to store end round stats from each end round file
    roundlist = []

    # used to count rounds played for formatting logic and display
    roundcount = 0
    rounddisplay = 1
    
    # Iterate through directory and process only file names of passed matchID to round list
    fileID = 1
    filesearch = re.escape(str(finished_matchID)) + r".*\.txt"
    matchfiles = [matchfile for matchfile in os.listdir(stats_dir_path) if re.match(filesearch, matchfile)]
    for matchfile in matchfiles:
        print("Adding file " + matchfile + " to match...")
        fileID += 1
        roundlist.append(matchfile)
        game = webstats_GameStats.get_game(stats_dir_path,matchfile)
        gamestats = webstats_GameStats.get_game_stats(stats_dir_path,matchfile)

    # capture config format for string matching
    configformat = str(game.config)
    print ("Game Config: " + configformat)

   # convert config used to a shortname / channel format set in web_config
    for display, config in matchconfigs.items():
        if config == configformat:
            matchformat = display
            break  # keep pair in the dictionary
        else:
            matchformat = webstats_config.DEFAULT_DISPLAY_FORMAT

    # output to check config matching
    print ("Match Channel: " + matchformat)

    # get a clean map name from each file name stored in round list
    for mapfile in roundlist:
        # this string split is based on formatting from the game-stats.lua module that generates end round stat file names
        fullfile, filext = mapfile.split(".txt",1)
        # split into usable strings
        matchid, year, month, dateplayed, timeplayed, mapname, roundtitle, roundid = fullfile.split("-",7)
        # only store unique map names, in this case every other round
        if (int(roundid) % 2) == 0:
            print("Map: " + mapname)
            maplist.append(mapname)

    # convert map list to string for display in file name output
    mapsplayed = '-'.join(maplist)

    # format server for display in file name
    serverdisplay = str(game.servername)
    print("Match Server: " + serverdisplay)

    # this string split is custom for Moe's Tavern server naming conventions - comment out following 2 lines to write full sv_hostname
    host, mod, svtype, region, serverid = serverdisplay.split(" ",4)
    serverdisplay = region+"-"+serverid

    # create file name for HTML output
    filename = str(matchid)+"-"+str(matchformat)+"-"+str(serverdisplay)+"-"+str(mapsplayed)+".html"
    fileout = str(output_dir) +"/"+ str(filename)

    # begin writing of HTML
    with open(fileout, 'w') as f:
        f.write("<!DOCTYPE html>\n<html>\n\n\t<head>\n\t\t<title>"+str(page_title)+"</title>\n\t\t<link rel=\"stylesheet\" href=\""+str(stylesheet_path)+"\">\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n\t</head>\n\n\t<body>\n")
        f.write("\n\t\t<div id=\"header\">\n\t\t\t<img src=\""+str(header_image)+"\">\n\t\t\t<h3><a href=\""+str(site_index)+"\">Back to Match List</a></h3>\n\t\t\t<h2>Stats for Match: "+str(matchid)+"</h2>")
        f.write("\n\t\t\t<h3>Server: "+str(game.servername)+"</h3>\n")
        f.write("\t\t</div>\n\n\t\t<br>\n\n")

        roundcount = 1
        mapnum = 1

        # Iterate through end-round stats files stored in round list
        for round in roundlist:
            roundmod = roundcount % 2
            playerid = 0

            game = webstats_GameStats.get_game(stats_dir_path,round)
            gamestats = webstats_GameStats.get_game_stats(stats_dir_path,round)

            # output for 1st round of each map
            if roundmod > 0:

                f.write("\t\t<div class=\"map map"+str(mapnum)+"\">\n")
                f.write("\n\t\t\t<h4 class=\"mapname\"><span>Map: "+str(game.mapname)+"</span></h4>\n\n\t\t\t<br>\n\t\t\t<br>\n\n")
                f.write("\t\t\t<div class=\"maprounds map"+str(mapnum)+"rounds\">")
                f.write("\n\n\t\t\t\t<div class=\"round round"+str(rounddisplay)+" map"+str(mapnum)+"round"+str(rounddisplay)+"\">\n")

                # generate team stats
                print("Writing game stats for Map "+str(mapnum)+" Round "+str(rounddisplay)+" to HTML...")
                for stats in gamestats:
                    f.write(stats)

                # generate player stats
                f.write("\n\t\t\t\t<div class=\"player-container\">\n")

                for players in game.players:
                    f.write(webstats_GameStats.get_player_stats(stats_dir_path,round,playerid))
                    playerid += 1

                f.write("\n\t\t\t\t</div>\n\t\t\t</div>\n")

                roundcount += 1
                rounddisplay += 1

            # output for 2nd round of each map
            else:
                f.write("\n\t\t\t<div class=\"round round"+str(rounddisplay)+" map"+str(mapnum)+"round"+str(rounddisplay)+"\">\n")

                #game = GameStatsWeb.get_game(dir_path,round)
                #gamestats = GameStatsWeb.get_game_stats(dir_path,round)

                # generate team stats
                print("Writing game stats for Map "+str(mapnum)+" Round "+str(rounddisplay)+" to HTML...")
                for stats in gamestats:
                    f.write(stats)

                # generate player stats
                f.write("\n\t\t\t\t<div class=\"player-container\">\n")

                for players in game.players:
                    f.write(webstats_GameStats.get_player_stats(stats_dir_path,round,playerid))
                    playerid += 1

                f.write("\n\n\t\t\t\t\t</div>\n")
                f.write("\n\t\t\t\t</div>\n\n\t\t\t</div>\n")

                mapnum += 1
                roundcount += 1
                rounddisplay = 1

        f.write("\n\n\t\t</div>\n\n\t</body>\n</html>")

        # remove local end round stats file after processing HTML output
        # os.remove(round)
