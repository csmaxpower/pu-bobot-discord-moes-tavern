# Settings for web host where stat files will be uploaded to
STATSWEBHOST=""
STATSWEBUSER=""
STATSWEBPASS=""
STATS_UPLOAD_DIR=""

# Game server FTP login and stats directory settings
GS_PATH="/legacy/gamestats/"
GS_FILE="matchid.txt"
FTP_TIMEOUT=5

# HTML Output and Directory Settings
PAGE_TITLE=""
INDEX_HOME_PATH=""
HEADER_IMAGE_PATH=""
STYLESHEET_DIR_PATH=""

# Local directories for stats processing
STATFILES_DL_PATH=""
OUTPUT_DIR=""

# Default game config display option
DEFAULT_DISPLAY_FORMAT="6v6"

# Dictionary for mapping game configs with desired visual output in the form of (display : config)
CONFIG_DISPLAY_FORMAT = {
  '1v1': 'legacy1',
  '3v3': 'legacy3',
  '5v5': 'legacy5',
  '6v6': 'legacy6',
  'Rifle Tennis': 'etlrifletennis',
  'Practice': 'practice'
}