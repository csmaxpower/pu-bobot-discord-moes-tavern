import discord
from discord.ext import commands, ipc
from discord.ext.ipc.server import Server
from modules import bot, stats3
import asyncio

from modules.client import get_member_by_id
from modules.exceptions import PubobotException


def member_list_gen(member_list):
    if member_list is None:
        return []
    return [{'id': str(member.id), 'nick': member.nick or member.name} for member in member_list]


def pickup_change_json(pickup):
    return {
        'channel_id': str(pickup.channel.id),
        'name': pickup.name,
        'size': pickup.cfg['maxplayers'],
        'players': member_list_gen(pickup.players),
    }


def pickup_json(pickup, user_id):
    return {
        'channel_id': str(pickup.channel.id),
        'name': pickup.name,
        'size': pickup.cfg['maxplayers'],
        'players': member_list_gen(pickup.players),
        'channel_name': pickup.channel.name
    }


def match_json(match):
    # TODO: why is id str???
    # TODO: send channel link?, add id, int(start_time), unpicked_maps
    return {
            'id': match.id,
            'channel_id': str(match.channel.id), 'pickup': match.pickup.name,
            'players': member_list_gen(match.players),
            'state': match.state,
            'unpicked': [] if match.unpicked is None else member_list_gen(match.unpicked),
            'alpha_draw': match.alpha_draw,
            'beta_draw': match.beta_draw,
            'require_ready': match.require_ready,
            'alpha_team': member_list_gen(match.alpha_team),
            'beta_team': member_list_gen(match.beta_team),
            'ranked': match.ranked,
            'ranks': match.ranks if match.ranked else None,
            'start_time': int(match.start_time),
            'unpicked_maps': match.unpicked_maps,
            'map_pick_order': match.map_pick_order,
            'picked_maps': match.picked_maps,
            'maps': match.maps,
            'players_ready': match.players_ready if match.state == 'waiting_ready' else [],
            'pick_order': match.pick_order,
            'pick_step': match.pick_step if hasattr(match, 'pick_step') else None
            }


def map_pick_json(match):
    return {
        'id': match.id,
        'players': member_list_gen(match.players),
        'unpicked_maps': match.unpicked_maps,
        'picked_maps': match.picked_maps,
        'map_pick_order': match.map_pick_order
    }


def player_pick_json(match):
    return {
        'id': match.id,
        'players': member_list_gen(match.players),
        'alpha_team': member_list_gen(match.alpha_team),
        'beta_team': member_list_gen(match.beta_team),
        'pick_step': match.pick_step,
        'unpicked': member_list_gen(match.unpicked)
    }


def set_ready_json(match):
    return {
        'id': match.id,
        'players': member_list_gen(match.players),
        'players_ready': [str(element) for element in match.players_ready]
    }


class IpcRoutes(commands.Cog):
    def __init__(self, discord_bot):
        self.bot = discord_bot
        if not hasattr(discord_bot, "ipc"):
            discord_bot.ipc = ipc.Server(self.bot, host="127.0.0.1", standard_port=2300, secret_key=discord_bot.client_config.IPC_SECRET)

    async def cog_load(self) -> None:
        await self.bot.ipc.start()

    async def cog_unload(self) -> None:
        await self.bot.ipc.stop()
        self.bot.ipc = None

    @commands.Cog.listener()
    async def on_ipc_ready(self):
        print("Ipc is ready")

    @Server.route()
    async def get_member_count(self, data):
        guild = self.bot.get_guild(data.guild_id)  # get the guild object using parsed guild_id

        return {'data': guild.member_count}  # return the member count to the client

    @Server.route()
    async def get_match(self, data):
        for match in bot.active_matches:
            if data.user_id in [player.id for player in match.players]:
                return match_json(match)
        return None

    # @Server.route()
    # async def set_ready(self, data):
    #     for channel in bot.channels:
    #         for match in bot.active_matches:
    #             if data.user_id in [player.id for player in match.players]:
    #                 # TODO: do this without retrieving member with api call
    #                 # member = await get_member_by_id(channel, str(data.user_id))
    #                 member = channel.channel.guild.get_member(data.user_id)
    #                 if member is not None:
    #                     channel.set_ready(member, data.is_ready)
    #                     return {"status": "ok"}
    #                 else:
    #                     return {"error": "not found in server members"}
    #     return {"error": "No active match found"}

    @Server.route()
    async def add(self, data):
        for channel in bot.channels:
            if channel.id == data.channel_id:
                target_pickups = [data.pickup_name]
                member = channel.channel.guild.get_member(data.user_id)
                if member is not None:
                    try:
                        channel.add_player(member, target_pickups)
                    except Exception as e:
                        return {"error": "unknown error"}
                    else:
                        return {"status": "ok?"}
                else:
                    return {"error": "not found in server members"}
        return {"error": "channel not found"}

    @Server.route()
    async def remove(self, data):
        for channel in bot.channels:
            if channel.id == data.channel_id:
                target_pickups = [data.pickup_name]
                member = await channel.channel.guild.fetch_member(data.user_id)
                if member is not None:
                    try:
                        channel.remove_player(member, target_pickups)
                    except Exception as e:
                        return {"error": "unknown error"}
                    else:
                        return {"status": "ok?"}
                else:
                    return {"error": "not found in server members"}
        return {"error": "channel not found"}

    @Server.route()
    async def get_pickups(self, data):
        # TODO:
        # add channel_id
        # add 'can_write' / 'can_add' attr, alongside with reason if can't
        # (no write perm, not in whitelist, in blacklist, banned, in active match)
        # send "active_pickups_changed" atleast when: someone is added/removed, match started/ended, banned/forgiven
        # role changed, permission changed
        # cache this per user somehow on webserver?

        # guilds_member = dict()

        def generate():
            for channel in bot.channels:
                if any([data.user_id in [player.id for player in pickup.players] for pickup in channel.pickups]):
                    for pickup in channel.pickups:
                        yield pickup_json(pickup, data.user_id)
        try:
            return {'data': [i for i in generate()]}
        except Exception as e:
            return {'data': []}

    @Server.route()
    async def get_active_matches(self, data):
        return {'data': [match.id for match in bot.active_matches]}

    @Server.route()
    async def get_user_stats(self, data):
        return {'data': stats3.get_user_stats(data.user_id)}

    @Server.route()
    async def get_user_pickups(self, data):
        index = data.index if hasattr(data, "index") else 0
        return {'data': stats3.get_user_pickups(data.user_id, data.channel_id, index)}

    @Server.route()
    async def get_leader_board(self, data):
        index = data.index if hasattr(data, "index") else 0
        # TODO: check for channel read permission - data.user_id
        rows = stats3.get_ladder(data.channel_id, index)
        return {'data': [[column for column in row] for row in rows]}

    @Server.route()
    async def pick_player(self, data):
        try:
            member, match = bot.find_member_in_active_matches(data.user_id)
        except StopIteration:
            return {"error": "not in active match"}
        try:
            match.pickup.channel.pick_player(member, int(data.pick_user_id), match)
            return {"status": "ok"}
        except PubobotException as e:
            return {'error': str(e)}
# TODO: route for getting data from stats3 like rank, lb, pickup activity


async def setup(discord_bot):
    print('I am being loaded!')
    await discord_bot.add_cog(IpcRoutes(discord_bot))
