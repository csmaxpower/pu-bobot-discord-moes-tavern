from discord import Embed

from modules import client, stats3, console
from modules.exceptions import *
import traceback
import re


def parse_user_id(arg):
    match = re.search(r"[0-9]+", arg)
    if match is not None:
        try:
            return match.group(0)
        except (IndexError, TypeError):
            console.display("ERROR| parse_user_id: {}".format(traceback.format_exc()))
            raise PubobotException("failed to parse user_id")
    else:
        raise PubobotException("failed to parse user_id")


def member_info_str(member_info):
    return f":flag_{member_info[3]}: `{member_info[2]}`"


def process_command(channel, member, cmd, args, access_level):
    try:
        if cmd in ["get_member_info", "gmi"]:
            try:
                member_info = stats3.get_member_info(parse_user_id(args[1]), channel.channel.guild.id)
            except IndexError:
                raise PubobotException("missing mention argument")
            if member_info is not None:
                client.notice(channel.channel, member_info_str(member_info))
                return True
            else:
                raise PubobotException("member_info not found")
        elif cmd in ["list_member_info", "lmi"]:
            try:
                page = int(args[1])
            except (ValueError, IndexError):
                page = 0
            embed = Embed()
            member_info_list = stats3.list_member_info(channel.channel.guild.id, page)
            embed.add_field(name="user", value='\n'.join([f"<@{info['user_id']}>" for info in member_info_list]), inline=True)
            embed.add_field(name="nick", value='\n'.join([info['nick'] for info in member_info_list]), inline=True)
            embed.add_field(name="country", value='\n'.join([f":flag_{info['country']}:" for info in member_info_list]), inline=True)

            if member_info_list:
                client.notice(channel.channel, msg=None, embed=embed)
            else:
                client.notice(channel.channel, msg="No records found")
            return True
        elif cmd in ["set_member_info", "smi"]:
            if access_level <= 0:
                raise MissingModeratorRightsException
            try:
                nick = args[2] if args[2].lower() != "none" else None
            except IndexError:
                raise PubobotException("missing nick argument")
            try:
                country = args[3][:2] if args[3].lower() != "none" else None
            except IndexError:
                raise PubobotException("missing country argument")

            user_id = parse_user_id(args[1])
            member_info = (user_id, channel.channel.guild.id, nick, country)
            stats3.set_member_info(*member_info)
            client.notice(channel.channel, f"member_info set. {member_info_str(member_info)}")
            return True
    except PubobotException as e:
        client.notice(channel.channel, str(e))
        return True
    return False
