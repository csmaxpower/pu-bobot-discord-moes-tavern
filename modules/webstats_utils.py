from enum import Enum
import re

class Arg:
    def __iter__(self):
        self.a = 0
        return self

    def __next__(self):
        self.a += 1
        return self.a


class Team(Enum):
    TEAM_FREE      = 0
    TEAM_AXIS      = 1
    TEAM_ALLIES    = 2
    TEAM_SPECTATOR = 3


def strip_color_codes(str):
    return re.sub("\^.", "", str)


weapon_stats = {
    0 : 
    {
        "name": "Knife",
        "has_headshots": False
    },
    1 : 
    {
        "name": "Ka-Bar",
        "has_headshots": False
    },
    2 : 
    {
        "name": "Luger",
        "has_headshots": True
    },
    3 : 
    {
        "name": "Colt",
        "has_headshots": True
    },
    4 : 
    {
        "name": "MP 40",
        "has_headshots": True
    },
    5 : 
    {
        "name": "Thompson",
        "has_headshots": True
    },
    6 : 
    {
        "name": "Sten",
        "has_headshots": True
    },
    7 : 
    {
        "name": "FG 42",
        "has_headshots": True
    },
    8 : 
    {
        "name": "Panzer",
        "has_headshots": False
    },
    9 : 
    {
        "name": "Bazooka",
        "has_headshots": False
    },
    10 : 
    {
        "name": "F.Thrower",
        "has_headshots": False
    },
    11 : 
    {
        "name": "Grenade",
        "has_headshots": False
    },
    12 : 
    {
        "name": "Mortar",
        "has_headshots": False
    },
    13 : 
    {
        "name": "Granatwerf",
        "has_headshots": False
    },
    14 : 
    {
        "name": "Dynamite",
        "has_headshots": False
    },
    15 : 
    {
        "name": "Airstrike",
        "has_headshots": False
    },
    16 : 
    {
        "name": "Artillery",
        "has_headshots": False
    },
    17 : 
    {
        "name": "Satchel",
        "has_headshots": False
    },
    18 : 
    {
        "name": "G.Launchr",
        "has_headshots": False
    },
    19 : 
    {
        "name": "Landmine",
        "has_headshots": False
    },
    20 : 
    {
        "name": "MG 42 Gun",
        "has_headshots": False
    },
    21 : 
    {
        "name": "Browning",
        "has_headshots": False
    },
    22 : 
    {
        "name": "Garand",
        "has_headshots": True
    },
    23 : 
    {
        "name": "K43 Rifle",
        "has_headshots": True
    },
    24 : 
    {
        "name": "Scp.Garand",
        "has_headshots": True
    },
    25 : 
    {
        "name": "Scp.K43",
        "has_headshots": True
    },
    26 : 
    {
        "name": "MP 34",
        "has_headshots": True
    },
    27 : 
    {
        "name": "Syringe",
        "has_headshots": False
    }
}