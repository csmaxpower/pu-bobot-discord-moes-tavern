from modules import webstats_utils
import os.path

class Weapon:
    def __init__(self, weapon):
        self.weapon_id  = weapon
        self.hits      = 0
        self.shots     = 0
        self.kills     = 0
        self.deaths    = 0
        self.headshots = 0
        self.acc       = 0


class PlayerStats:
    def __init__(self):
        self.weapons = []

        self.total_shots     = 0
        self.total_hits      = 0
        self.total_kills     = 0
        self.total_deaths    = 0
        self.total_headshots = 0

        self.damage_given         = 0
        self.damage_received      = 0
        self.team_damage_given    = 0
        self.team_damage_received = 0
        self.gibs                 = 0
        self.self_kills           = 0
        self.team_kills           = 0
        self.team_gibs            = 0
        self.efficiency           = 0
        self.play_time_ratio      = 0
        self.hit_ratio            = 0
        self.headshot_ratio       = 0
        self.xp                   = 0

        self.has_stats = False


class Player:
    def __init__(self, id, guid, name, rounds, team):
        self.id     = id
        self.guid   = guid
        self.name   = name
        self.rounds = rounds
        self.team   = webstats_utils.Team(team)
        self.stats  = PlayerStats()


class GameStats:
    def __init__(self):
        self.total_kills                = 0
        self.total_deaths               = 0
        self.total_gibs                 = 0
        self.total_self_kills           = 0
        self.total_team_kills           = 0
        self.total_team_gibs            = 0
        self.total_damage_given         = 0
        self.total_damage_received      = 0
        self.total_team_damage_given    = 0
        self.total_team_damage_received = 0
        self.total_xp                   = 0
        self.total_play_time            = 0
        self.total_efficiency           = 0


class GameTeams:
    def __init__(self, team):
        self.team  = team
        self.stats = GameStats()


class Game:
    def set(self, servername, mapname, config, round, defender, winner, timelimit, nextTimelimit, matchID):
        self.matchID       = matchID
        self.servername    = webstats_utils.strip_color_codes(servername)
        self.mapname       = mapname
        self.config        = config
        self.round         = round
        self.defender      = webstats_utils.Team(defender)
        self.winner        = webstats_utils.Team(winner)
        self.timelimit     = timelimit
        self.nextTimelimit = nextTimelimit
        self.players       = []
        self.teams         = []
        for team in webstats_utils.Team:
            self.teams.append(GameTeams(team))


def sort_by_xp(players):
    return players.stats.xp


def get_game(arg1,arg2):

    filename = os.path.join(os.path.curdir, arg1, arg2)

    game = Game()
    id   = 0

    with open(filename) as file:
        header = file.readline().split('\\')
        game.set(header[0], header[1], header[2], header[3], int(header[4]), int(header[5]), header[6], header[7], header[8])

        for line in file:
            arg   = iter(webstats_utils.Arg())
            lines = line.strip().split('\\')

            guid   = lines[0]
            name   = webstats_utils.strip_color_codes(lines[1])
            rounds = lines[2]
            team   = int(lines[3])
            stats  = lines[4].split(' ')

            player = Player(id, guid, name, rounds, team)
            game.players.append(player)
            id += 1

            for k, v in webstats_utils.weapon_stats.items():
                if int(stats[0]) & (1 << k):
                    weapon = Weapon(k)
                    player.stats.weapons.append(weapon)

                    weapon.hits       = int(stats[next(arg)])
                    weapon.shots      = int(stats[next(arg)])
                    weapon.kills      = int(stats[next(arg)])
                    weapon.deaths     = int(stats[next(arg)])
                    weapon.headshots  = int(stats[next(arg)])
                    weapon.acc        = (weapon.hits * 100) / weapon.shots if weapon.shots > 0 else 0.0;

                    if v["has_headshots"]:
                        player.stats.total_shots     += weapon.shots
                        player.stats.total_hits      += weapon.hits
                        player.stats.total_headshots += weapon.headshots;

                    player.stats.total_kills  += weapon.kills;
                    player.stats.total_deaths += weapon.deaths;
                    player.stats.has_stats     = True

            if player.stats.has_stats:
                player.stats.damage_given         = int(stats[next(arg)])
                player.stats.damage_received      = int(stats[next(arg)])
                player.stats.team_damage_given    = int(stats[next(arg)])
                player.stats.team_damage_received = int(stats[next(arg)])
                player.stats.gibs                 = int(stats[next(arg)])
                player.stats.self_kills           = int(stats[next(arg)])
                player.stats.team_kills           = int(stats[next(arg)])
                player.stats.team_gibs            = int(stats[next(arg)])
                player.stats.play_time_ratio      = float(stats[next(arg)])
                player.stats.xp                   = int(stats[next(arg)])

                player.stats.efficiency     = 0 if (player.stats.total_deaths + player.stats.total_kills == 0) else int(100 * player.stats.total_kills / (player.stats.total_deaths + player.stats.total_kills))
                player.stats.hit_ratio      = 0 if (player.stats.total_shots == 0) else (player.stats.total_hits * 100.0 / player.stats.total_shots)
                player.stats.headshot_ratio = 0 if (player.stats.total_hits == 0) else (player.stats.total_headshots * 100.0 / player.stats.total_hits)

                game.teams[player.team.value].stats.total_kills                += player.stats.total_kills
                game.teams[player.team.value].stats.total_deaths               += player.stats.total_deaths
                game.teams[player.team.value].stats.total_gibs                 += player.stats.gibs
                game.teams[player.team.value].stats.total_self_kills           += player.stats.self_kills
                game.teams[player.team.value].stats.total_team_kills           += player.stats.team_kills
                game.teams[player.team.value].stats.total_team_gibs            += player.stats.team_gibs
                game.teams[player.team.value].stats.total_damage_given         += player.stats.damage_given
                game.teams[player.team.value].stats.total_damage_received      += player.stats.damage_received
                game.teams[player.team.value].stats.total_team_damage_given    += player.stats.team_damage_given
                game.teams[player.team.value].stats.total_team_damage_received += player.stats.team_damage_received
                game.teams[player.team.value].stats.total_play_time            += player.stats.play_time_ratio
                game.teams[player.team.value].stats.total_xp                   += player.stats.xp
                game.teams[player.team.value].stats.total_efficiency            = 0 if (game.teams[player.team.value].stats.total_deaths + game.teams[player.team.value].stats.total_kills == 0) else int(100 * game.teams[player.team.value].stats.total_kills / (game.teams[player.team.value].stats.total_deaths + game.teams[player.team.value].stats.total_kills))

    return game


def get_game_stats(arg1,arg2):
    game = get_game(arg1,arg2)
    game.players.sort(key = sort_by_xp, reverse = True)

    gamestats = []

    stats = "\n\t\t\t\t\t<table class=\"roundno roundinfo\">\n"
    stats += "\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<th>Round:</th>\n\t\t\t\t\t\t</tr>\n\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t</tr>\n\n\t\t\t\t\t</table>\n".format(game.round)
    stats += "\n\t\t\t\t\t<table class=\"roundinfo\">\n"
    stats += "\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<th>Config:</th>\n\t\t\t\t\t\t\t<th>Defender:</th>\n\t\t\t\t\t\t\t<th>Winner:</th>\n\t\t\t\t\t\t\t<th>Timelimit:</th>\n\t\t\t\t\t\t\t<th>NextTimelimit:</th>\n\t\t\t\t\t\t</tr>\n\n"
    stats += "\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</table>\n\n".format(game.config, game.defender.name[5:], game.winner.name[5:], game.timelimit, game.nextTimelimit.strip())
    stats += "\n\t\t\t\t\t<div class=\"largetable\">\n\t\t\t\t\t\t<table class=\"roundinfo\">\n"
    stats += "\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<th>ID</th>\n\t\t\t\t\t\t\t\t<th>Team</th>\n\t\t\t\t\t\t\t\t<th>Player</th>\n\t\t\t\t\t\t\t\t<th>Kll</th>\n\t\t\t\t\t\t\t\t<th>Dth</th>\n\t\t\t\t\t\t\t\t<th>Gib</th>\n\t\t\t\t\t\t\t\t<th>SK</th>\n\t\t\t\t\t\t\t\t<th>TK</th>\n\t\t\t\t\t\t\t\t<th>TG</th>\n\t\t\t\t\t\t\t\t<th>Eff</th>\n\t\t\t\t\t\t\t\t<th>DG</th>\n\t\t\t\t\t\t\t\t<th>DR</th>\n\t\t\t\t\t\t\t\t<th>TDG</th>\n\t\t\t\t\t\t\t\t<th>TDR</th>\n\t\t\t\t\t\t\t\t<th>Score</th>\n\t\t\t\t\t\t\t</tr>\n"

    for team in game.teams[1:]:
        for player in game.players:
            if player.team == team.team and player.stats.has_stats:
                stats += "\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t</tr>\n".format(
                    player.id, player.team.name[5:], player.name, player.stats.total_kills, player.stats.total_deaths,
                    player.stats.gibs, player.stats.self_kills, player.stats.team_kills,
                    player.stats.team_gibs, player.stats.efficiency, player.stats.damage_given, player.stats.damage_received,
                    player.stats.team_damage_given, player.stats.team_damage_received, player.stats.xp)

        stats += "\n\t\t\t\t\t\t\t<tr class=\"totals\">\n\t\t\t\t\t\t\t\t<td>-</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>Totals</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t</tr>".format(
            team.team.name[5:], team.stats.total_kills, team.stats.total_deaths, team.stats.total_gibs, team.stats.total_self_kills, team.stats.total_team_kills,
           team.stats.total_team_gibs, team.stats.total_efficiency, team.stats.total_damage_given, team.stats.total_damage_received, team.stats.total_team_damage_given,
          team.stats.total_team_damage_received, team.stats.total_xp)

        gamestats.append(stats)
        stats = ""

    stats += "\n\t\t\t\t\t</table>\n\t\t\t\t</div>\n"
    gamestats.append(stats)

    return gamestats


def get_player_stats(arg1,arg2,player):
    game = get_game(arg1,arg2)
    game.players.sort(key = sort_by_xp, reverse = True)

    index = int(player)

    if len(game.players) > index and index >= 0:
        for player in game.players:
            if player.id == index and player.stats.has_stats:
                stats = "\n\t\t\t\t\t<div class=\"player largetable\">\n\n\t\t\t\t\t\t<table class=\"playertablelarge\">\n"
                stats += "\t\t\t\t\t\t\t<caption><b>Name: </b>{}<b> Team: </b>{}<b> Round: </b>{}</caption>\n".format(player.name, player.team.name[5:], player.rounds)
                stats += "\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<th>Weapon</th>\n\t\t\t\t\t\t\t\t<th>Acrcy</th>\n\t\t\t\t\t\t\t\t<th>Hits/Shts</th>\n\t\t\t\t\t\t\t\t<th>Kills</th>\n\t\t\t\t\t\t\t\t<th>Deaths</th>\n\t\t\t\t\t\t\t\t<th>Headshots</th>\n\t\t\t\t\t\t\t</tr>\n"

                for weapon in player.stats.weapons:
                    stats += "\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<td><b>{}</b></td>\n\t\t\t\t\t\t\t\t<td>{:<7.1f}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t</tr>\n".format(
                        webstats_utils.weapon_stats[weapon.weapon_id]["name"], weapon.acc, "{}/{}".format(weapon.hits, weapon.shots), weapon.kills, weapon.deaths, weapon.headshots)

                stats += "\t\t\t\t\t\t</table>\n"
                stats += "\n\t\t\t\t\t\t<table class=\"playertablesmall\">\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<th>Damage Given:</th>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<th>Team Damage Given:</th>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t</tr>\n".format(player.stats.damage_given, player.stats.team_damage_given)
                stats += "\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<th>Damage Recvd:</th>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<th>Team Damage Recvd:</th>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t</table>\n".format(player.stats.damage_received, player.stats.team_damage_received)
                stats += "\n\t\t\t\t\t\t<table class=\"playertablesmall\">\n\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<th>Kills:</th>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<th>Team Kills:</th>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<th>Accuracy:</th>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t</tr>\n".format(player.stats.total_kills, player.stats.team_kills, "{0:0.1f}%".format(player.stats.hit_ratio))
                stats += "\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<th>Deaths:</th>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<th>Self Kills:</th>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<th>Headshots:</th>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t</tr>\n".format(player.stats.total_deaths, player.stats.self_kills, "{0:0.1f}%".format(player.stats.headshot_ratio))
                stats += "\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t<th>Gibs:</th>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<th>Team Gibs:</th>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t\t<th>Playtime:</th>\n\t\t\t\t\t\t\t\t<td>{}</td>\n\t\t\t\t\t\t\t</tr>\n".format(player.stats.gibs, player.stats.team_gibs, "{0:0.1f}%".format(float(player.stats.play_time_ratio)))
                stats += "\n\t\t\t\t\t\t</table>\n\t\t\t\t\t</div>\n"

                return stats
    else:
        return "Bad argument"

    return "No stats for given player"
