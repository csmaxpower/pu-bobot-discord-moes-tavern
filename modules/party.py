from modules import stats3, scheduler
from modules.exceptions import PubobotException
from time import time
from datetime import timedelta


class Party:

    def __init__(self, captain, pickup, members=None):
        if captain is not None:
            self.members = [captain]
            if members is not None:
                self.members += members
        else:
            self.members = []
        # TODO make use of pickup groups, use set of pickups
        self.pickup = pickup
        self.capacity = self.pickup.cfg['maxplayers'] // 2
        self.invite_list = []
        self.challenger = None
        self.challenged = None
        self.modification_time = None
        self.scheduler_task_name = None
        self.gen_scheduler_task_name()
        self.touch()
        Party.party_counter += 1

    party_counter = 0

    @staticmethod
    def invite(captain, player, channel):
        try:
            # TODO: possibility to specify pickup if there are multiple
            party = Party.find_party_by_player(captain, channel)
        except NotInPartyException:
            party = Party.create_party(captain, None, channel)
        if not party.get_captain() == captain:
            raise PartyNotCaptainException
        if party.is_player_invited(player):
            raise AlreadyInvitedException
        party.invite_list.append(player)
        party.touch()

    @staticmethod
    def get_players_invitation(player, channel):
        return [party for party in channel.active_parties if player in party.invite_list]

    @staticmethod
    def get_player_invite_by_party_idx(player, party_idx, channel):
        parties = Party.get_players_invitation(player, channel)
        if len(parties) == 0:
            raise PubobotException("You are not invited to any party.")
        elif len(parties) > 1:
            if party_idx is None:
                raise PubobotException("You got multiple invites."
                                       " Specify party number."
                                       " Use !parties to see party numbers")
            else:
                try:
                    return channel.active_parties[party_idx]
                except IndexError:
                    raise PartyIndexNotFound(party_idx)
        else:
            return parties[0]

    @staticmethod
    def accept_invite(player, channel, party_idx=None):
        party = Party.get_player_invite_by_party_idx(player, party_idx, channel)
        try:
            old_party = Party.find_party_by_player(player, channel)
            old_party.remove_player(player)
        except NotInPartyException:
            pass
        party.add_player(player)
        party.invite_list.remove(player)
        return party

    @staticmethod
    def decline_invite(player, channel, party_idx=None):
        party = Party.get_player_invite_by_party_idx(player, party_idx, channel)
        party.invite_list.remove(player)
        return party

    @staticmethod
    def kick(captain, channel, player):
        party = Party.find_party_by_player(captain, channel)
        if isinstance(party, PublicParty):
            raise PubobotException("Can't kick players from public party")
        if not party.get_captain() == captain:
            raise PartyNotCaptainException
        if player not in party.members:
            raise NotInPartyException
        party.remove_player(player)

    def __str__(self):
        ret = '[**{}** ({}/{})] {}'.format(self.pickup.name, len(self.members), self.capacity,
                                           '/'.join(
                                               ["`" + (i.nick or i.name).replace("`", "") + "`" for i
                                                in self.members]
                                           )
                                           )
        if self.challenged is not None:
            ret += " challenged #{}".format(self.pickup.channel.active_parties.index(self.challenged))
        task = scheduler.tasks.get(self.scheduler_task_name)
        if task is not None:
            ret += " TTL: {}".format(str(timedelta(seconds=int(task[0] - time()))))
        return ret

    def challenge(self, captain, channel, party_idx=None):
        if not self.get_captain() == captain:
            raise PartyNotCaptainException
        if not self.is_full():
            raise PartyChallengeNotFullException
        try:
            if party_idx is None:
                tmp = channel.active_parties.copy()
                tmp.remove(self)
                target_party = tmp[0]
            else:
                target_party = channel.active_parties[party_idx]
        except IndexError:
            raise PartyIndexNotFound(party_idx)

        if self == target_party:
            raise PubobotException("Can't challenge your own party")
        if self.pickup != target_party.pickup:
            raise PartyChallengeDifferentPickupException
        if isinstance(target_party, PickupParty):
            raise PubobotException("Can't challenge pickup party")
        if target_party.challenger is not None:
            raise PubobotException("This party already got challenger")
        if not target_party.is_full():
            raise PartyChallengeNotFullException
        # TODO schedule challenge expiration
        target_party.challenger = self
        self.touch()
        self.challenged = target_party
        return target_party

    @staticmethod
    def accept_challenge(captain, channel):
        party = Party.find_party_by_player(captain, channel)
        if not party.get_captain() == captain:
            raise PartyNotCaptainException
        if party.challenger is None:
            return
        if not party.is_full() or not party.challenger.is_full:
            raise PartyChallengeNotFullException
        players_in_active_match = [
            member for member in party.members + party.challenger.members
            if party.pickup.channel.match_by_player(member) is not None
        ]
        if len(players_in_active_match):
            raise PartyPlayersInMatchException(players_in_active_match)
        party.pickup.channel.start_pickup(party.pickup, [party, party.challenger])
        party.touch()
        party.challenger.touch()
        party.challenger.challenged = None
        party.challenger = None

    @staticmethod
    def decline_challenge(captain, channel):
        party = Party.find_party_by_player(captain, channel)
        if not party.get_captain() == captain:
            raise PartyNotCaptainException
        party.touch()
        if party.challenger is None:
            raise PubobotException("Your party isn't being challenged")
        challenger = party.challenger
        party.challenger = None
        challenger.challenged = None
        return challenger

    def get_captain(self):
        return self.members[0]

    def is_full(self):
        return len(self.members) == self.capacity

    @staticmethod
    def who(channel):
        parties = [party for party in channel.active_parties if party.pickup in channel.pickups]
        ret = 'Parties:\n' if len(parties) else 'No parties'
        ret += '\n'.join(['#{} {}'.format(idx, str(party)) for idx, party in enumerate(parties)])
        return ret

    def is_in_party(self, player):
        return player in self.members

    @staticmethod
    def is_in_any_party(player, channel):
        return any([player in party.members for party in channel.active_parties])

    def is_player_invited(self, player):
        return player in self.invite_list

    @staticmethod
    def find_party_by_player(player, channel):
        try:
            return next(party for party in channel.active_parties if player in party.members)
        except StopIteration:
            raise NotInPartyException

    def add_player(self, player):
        result = stats3.check_memberid(self.pickup.channel.id, player.id)  # is_banned, phrase, default_expire
        if result[0]:  # if banned
            raise PubobotException(result[1])
        if not self.is_full():
            self.members.append(player)
            self.touch()
        else:
            raise PartyFullException

    def remove_player(self, player):
        self.members.remove(player)
        self.touch()
        if len(self.members) == 0:
            Party.remove_party(self)
            return None
        return self

    @staticmethod
    def remove_party(party):
        if party.challenged is not None:
            party.challenged.challenger = None
        if party.challenger is not None:
            party.challenger.challenged = None
        if scheduler.task_exists(party.scheduler_task_name):
            scheduler.cancel_task(party.scheduler_task_name)
        party.pickup.channel.active_parties.remove(party)

    @staticmethod
    def add_party(player, channel, party_idx):
        try:
            channel.active_parties[party_idx].add_player(player)
        except IndexError:
            raise PartyIndexNotFound(party_idx)

    @staticmethod
    def get_pickup_by_name(pickup_name, channel):
        if pickup_name is None:
            if len(channel.pickups) == 1:
                pickup = channel.pickups[0]
            else:
                raise PubobotException("Specify pickup")
        else:
            try:
                pickup = next(pickup for pickup in channel.pickups if pickup.name.lower() == pickup_name)
            except StopIteration:
                raise PubobotException("Pickup not found")
        return pickup

    @staticmethod
    def create_party(captain, pickup_name, channel, pickup_party=False):
        if Party.is_in_any_party(captain, channel):
            raise AlreadyInPartyException
        # check noadds
        result = stats3.check_memberid(channel.id, captain.id)  # is_banned, phrase, default_expire
        if result[0]:  # if banned
            raise PubobotException(result[1])
        pickup = Party.get_pickup_by_name(pickup_name, channel)
        if pickup_party:
            party = PickupParty(captain, pickup)
        else:
            party = Party(captain, pickup)
        channel.active_parties.append(party)
        return party

    def status_message_str(self):
        return '[**{}** ({}/{})]'.format(self.pickup.name, len(self.members), self.capacity)

    def update_status_message(self):
        self.pickup.channel.update_status_message(
            'party_msg',
            '**Parties**',
            ' '.join([p.status_message_str() for p in
                      self.pickup.channel.active_parties])
        )

    def touch(self):
        task_name = self.scheduler_task_name

        def remove_party_callback():
            Party.remove_party(self)

        if len(self.members) < self.capacity * 2 / 3:
            scheduler.add_task(task_name, 60 * 60 * len(self.members), remove_party_callback)
        else:
            if task_name in scheduler.tasks:
                scheduler.cancel_task(task_name)
            scheduler.add_task(task_name, 60 * 60 * 24 * 2 * len(self.members) // self.capacity, remove_party_callback)
        self.modification_time = time()
        self.update_status_message()

    def gen_scheduler_task_name(self):
        self.scheduler_task_name = "party-{}-expire".format(Party.party_counter)


class PublicParty(Party):
    def __str__(self):
        return "**Public Party** - " + super().__str__()

    def challenge(self, captain, channel, party_idx=None):
        raise PubobotException("Public party can't challenge other parties")

    @staticmethod
    def get(pickup_name, channel):
        pickup = Party.get_pickup_by_name(pickup_name, channel)
        try:
            party = next(party for party in channel.active_parties
                         if isinstance(party, PublicParty) and party.pickup == pickup)
        except StopIteration:
            party = PublicParty(None, pickup)
            channel.active_parties.append(party)
        return party

    @staticmethod
    def add(player, pickup_name, channel):
        if Party.is_in_any_party(player, channel):
            raise AlreadyInPartyException
        party = PublicParty.get(pickup_name, channel)
        party.add_player(player)
        return party


class PickupParty(Party):
    def __str__(self):
        return "**Pickup Party** - " + super().__str__()

    def __init__(self, captain, pickup, members=None):
        super(PickupParty, self).__init__(captain, pickup, members)
        self.capacity = self.pickup.cfg['maxplayers']

    def challenge(self, captain, channel, party_idx=None):
        raise PubobotException("Pickup party can't challenge other parties")

    # TODO: dunno if its best idea to auto-start when its full
    # def add_player(self, player):
    #     super(PickupParty, self).add_player(player)
    #     if self.is_full():
    #         self.start_pickup()

    def start_pickup(self):
        if self.is_full():
            self.pickup.channel.start_pickup(self.pickup, None, self.members)
        else:
            raise PartyChallengeNotFullException


# TODO: create team party from Party while Party stays as superclass?


class AlreadyInPartyException(PubobotException):
    def __str__(self):
        return "Already in party"


class NotInPartyException(PubobotException):
    def __str__(self):
        return "Not in party"


class AlreadyInvitedException(PubobotException):
    def __str__(self):
        return "Already invited"


class PartyFullException(PubobotException):
    def __str__(self):
        return "Party is full"


class PartyIndexNotFound(PubobotException):
    def __init__(self, party_idx):
        self.party_idx = party_idx

    def __str__(self):
        if self.party_idx is None:
            return "Party not found."
        else:
            return "Party #{} not found.".format(self.party_idx)


class PartyNotCaptainException(PubobotException):
    def __str__(self):
        return "You are not captain of party"


class PartyChallengeNotFullException(PubobotException):
    def __str__(self):
        return "Party has to be full 1st"


class PartyPlayersInMatchException(PubobotException):
    def __init__(self, players):
        self.players = players

    def __str__(self):
        if len(self.players) == 1:
            return "Party member {} is in active match.".format(self.players[0].nick or self.players[0].name)
        else:
            return "Party members #{} are in active match.".format(
                ', '.join([(player.nick or player.name) for player in self.players])
            )


class PartyChallengeDifferentPickupException(PubobotException):
    def __str__(self):
        return "Can't challenge party for different pickup"
