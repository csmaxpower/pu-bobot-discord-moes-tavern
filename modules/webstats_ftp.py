import os
import os.path
import re
from ftplib import FTP
from modules import webstats_app
from modules import webstats_config as statscfg

def send_matchid(server, matchID, ftp_user, ftp_pw):
    # connect to host, default port
    ftp = FTP(server, timeout=statscfg.FTP_TIMEOUT)
    # login with user and pass defined in config
    ftp.login(user=ftp_user, passwd=ftp_pw)
    # set the current working directory defined in config
    ftp.cwd(statscfg.GS_PATH)
    # create a file with name from config and write the match id inside
    filename = statscfg.GS_FILE
    open(filename, 'w').write(str(matchID))
    # Read file in binary mode
    with open(filename, "rb") as file:
        # put file on server of current match
        try:
            ftp.storbinary(f"STOR {filename}", file)
        except EOFError:
            pass
    # remove local match temp file
    os.remove(filename)
    # close ftp connection to server
    ftp.quit()

def cancel_match(server, cancelled_matchID, ftp_user, ftp_pw):
    # connect to host, default port
    ftp = FTP(server, timeout=statscfg.FTP_TIMEOUT)
    # login with user and pass defined in config
    ftp.login(user=ftp_user, passwd=ftp_pw)
    # change to directory where stats files are written to
    ftp.cwd(statscfg.GS_PATH)
    # List all files in directory and store them
    filesearch = re.escape(str(cancelled_matchID)) + r".*\.txt"
    matchfiles = [matchfile for matchfile in ftp.nlst() if re.match(filesearch, matchfile)]
    if len(matchfiles) == 0:
        # remove matchid file from game server
        try:
            ftp.delete(statscfg.GS_PATH + statscfg.GS_FILE)
        except:
            pass
    else:
        # Delete any stats files found to clean up game server
        for matchfile in matchfiles:
            try:
                print("Deleting from game server..." + matchfile)
                ftp.delete(statscfg.GS_PATH + matchfile)
            except EOFError:    # To avoid EOF errors.
                pass
        # remove matchid file from game server after cleaning up stats files
        try:
            ftp.delete(statscfg.GS_PATH + statscfg.GS_FILE)
        except:
            pass
    # close ftp connection to server
    ftp.quit()

def get_stats(server, finished_matchID, ftp_user, ftp_pw):
    # connect to host, default port
    ftp = FTP(server, timeout=statscfg.FTP_TIMEOUT)
    # login with user and pass defined in config
    ftp.login(user=ftp_user, passwd=ftp_pw)
    # change to directory where stats files are written to
    ftp.cwd(statscfg.GS_PATH)
    # List all files in directory and store them
    filesearch = re.escape(str(finished_matchID)) + r".*\.txt"
    matchfiles = [matchfile for matchfile in ftp.nlst() if re.match(filesearch, matchfile)]
    if len(matchfiles) == 0:
        # remove matchid file from game server even if no stats files are present
        try:
            ftp.delete(statscfg.GS_PATH + statscfg.GS_FILE)
        except:
            pass
        ftp.quit()
        return False
    else:
        # Download stats files and then remove them from game server to clean up
        for matchfile in matchfiles:
            try:
                print("Downloading from game server..." + matchfile)
                ftp.retrbinary("RETR " + matchfile ,open(statscfg.STATFILES_DL_PATH + matchfile, 'wb').write)
                print("Deleting from game server..." + matchfile)
                ftp.delete(statscfg.GS_PATH + matchfile)
            except EOFError:    # To avoid EOF errors.
                pass
    # remove matchid file from game server after downloading stats files
    try:
        ftp.delete(statscfg.GS_PATH + statscfg.GS_FILE)
    except:
        pass
    # close ftp connection to server
    ftp.quit()
    return True

def send_stats(finished_matchID):
    # connect to host, default port
    ftp = FTP(statscfg.STATSWEBHOST, timeout=statscfg.FTP_TIMEOUT)
    # login with user and pass defined in config
    ftp.login(user=statscfg.STATSWEBUSER, passwd=statscfg.STATSWEBPASS)
    # change to directory where stats files are written to
    ftp.cwd(statscfg.STATS_UPLOAD_DIR)
    # Iterate through directory and send only file names matching the matchID to web server
    filesearch = re.escape(str(finished_matchID)) + r".*\.html"
    htmlfiles = [htmlfile for htmlfile in os.listdir(statscfg.OUTPUT_DIR) if re.match(filesearch, htmlfile)]
    # send files
    for htmlfile in htmlfiles:
        # Read file in binary mode
        with open(statscfg.OUTPUT_DIR + htmlfile, "rb") as file:
            # upload html output to web server
            try:
                 print("Uploading..." + htmlfile)
                 ftp.storbinary(f"STOR {htmlfile}", file)
                 filename = htmlfile
            except EOFError:
                pass
        # delete local html output file after it has been uploaded to web server
        os.remove(htmlfile) 
    # close ftp connection to server
    ftp.quit()
    # return uploaded filename
    return filename

def process_stats(server, finsihed_matchID, ftp_user, ftp_pw):
    # download stats from game server
    statsfiles = get_stats(server, finsihed_matchID, ftp_user, ftp_pw)
    # if stats files were downloaded, then process them
    if statsfiles == True:
        # process stat files that were downloaded from game server for finished match
        webstats_app.build_stats(finsihed_matchID)
        # upload html output for finished match to stats web server
        webfile = send_stats(finsihed_matchID)
        # build full http link of file that was just uploaded
        hyperlink = webstats_app.site_index + webfile
    else:
        hyperlink = "No stats were found for this match.\n" + "Site Index: " + webstats_app.site_index
    # return full hyperlink for use in bot message output
    return hyperlink